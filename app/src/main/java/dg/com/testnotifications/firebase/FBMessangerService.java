package dg.com.testnotifications.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

import dg.com.testnotifications.R;
import dg.com.testnotifications.activity.MainActivity;
import dg.com.testnotifications.model.PushNotification;
import dg.com.testnotifications.utils.preferences.CorePreferencesImpl;

public class FBMessangerService extends FirebaseMessagingService {

    private final int NOTIFICATION_ID = 1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String title = remoteMessage.getNotification().getTitle();
        String message = remoteMessage.getNotification().getBody();
        Log.e("FCM TAG", title + " " + message);

        showNotification(title, message);
        saveNotification(new PushNotification(title, message, System.currentTimeMillis()));
    }

    private void showNotification(String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(this, "TEST CHANNEL ID");
            NotificationChannel channel = new NotificationChannel("TEST CHANNEL ID","CHANNEL NAME", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("CHANNEL DESCRIPTION");
            notificationManager.createNotificationChannel(channel);
        } else {
            builder = new NotificationCompat.Builder(this);
        }

        builder
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private void saveNotification(PushNotification notification) {
        List<PushNotification> notificationList = CorePreferencesImpl.getCorePref().getNotificationList();
        notificationList.add(notification);
        CorePreferencesImpl.getCorePref().setNotificationList(notificationList);
    }

}
