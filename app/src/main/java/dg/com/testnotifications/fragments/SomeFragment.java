package dg.com.testnotifications.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import dg.com.testnotifications.R;

import static dg.com.testnotifications.adapter.ViewPagerAdapter.PAGE_1;
import static dg.com.testnotifications.adapter.ViewPagerAdapter.PAGE_2;
import static dg.com.testnotifications.adapter.ViewPagerAdapter.PAGE_3;

public class SomeFragment extends Fragment {

    @BindView(R.id.textView) public TextView textView;
    @BindView(R.id.fragmentLayout) public ConstraintLayout fragmentLayout;

    public static final String KEY_PAGE_TYPE = "key.page.type";

    private int pageType;

    public static SomeFragment getInstance(int pageType) {
        SomeFragment fragment = new SomeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_PAGE_TYPE, pageType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromArguments();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.some_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFragmentBackground();
    }

    private void getDataFromArguments() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(KEY_PAGE_TYPE)) {
            pageType = bundle.getInt(KEY_PAGE_TYPE);
        }
    }

    private void setFragmentBackground() {
        fragmentLayout.setBackgroundColor(getFragmentColor());
    }

    private int getFragmentColor() {
        switch(pageType) {
            case PAGE_1: return Color.GRAY;
            case PAGE_2: return Color.BLUE;
            case PAGE_3: return Color.RED;
            default: return Color.BLACK;
        }
    }

}
