package dg.com.testnotifications.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dg.com.testnotifications.R;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginViaFacebookActivity extends AppCompatActivity {

    private CallbackManager callbackManager;

    @BindView(R.id.tvUserName) TextView tvUserName;
    @BindView(R.id.userImage) ImageView userImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_via_facebook);
        ButterKnife.bind(this);

        setupFacebook();
    }

    public void setupFacebook() {
        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Api call to server where you send only accessToken -> loginResult.getAccessToken()

                getProfileData(loginResult.getAccessToken());
                Log.e("TAG", "LogIn Success!!!");
            }

            @Override
            public void onCancel() {
                Log.e("TAG", "LogIn Success!!!");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e("TAG", "LogIn Success!!!");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void getProfileData(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    //Start parsing
                    String id = object.getString("id"); //Parse json object and getting data from field "id"
                    String avatar_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                    String fullName = object.getString("first_name") + " " + object.getString("last_name");
                    Log.e("TAG", "TAG");

                    tvUserName.setText(fullName);
                    //userImage.setImageURI(Uri.parse(avatar_url)); //TODO Show user avatar. Use Picasso or Glide
                    Picasso.get().load(avatar_url).into(userImage);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle requestParam = new Bundle();
        requestParam.putString("fields", "first_name,last_name,email,id");
        graphRequest.setParameters(requestParam);
        graphRequest.executeAsync();
    }

    @OnClick(R.id.ownFbBtn)
    public void clickFacebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void fbLogout() {
        LoginManager.getInstance().logOut();
    }

}
