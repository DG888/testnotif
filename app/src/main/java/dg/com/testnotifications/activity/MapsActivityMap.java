package dg.com.testnotifications.activity;

import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dg.com.testnotifications.R;
import dg.com.testnotifications.enums.City;

public class MapsActivityMap extends UserMapLocationActivity implements OnMapReadyCallback {

    private static final LatLng SYDNEY = new LatLng(-34,151);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.addMarker(new MarkerOptions().position(SYDNEY).title("Sydney"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, 10));

        enableMyLocationIfPermitted();
    }

    @Override
    public void handleCurrentLocationData(Location location) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
        Toast.makeText(getBaseContext(), "User location displayed on the map!!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnKyiv)
    public void onClickBtnKyiv() {
        addNewLocation(0);
    }

    @OnClick(R.id.btnKharkiv)
    public void onClickBtnKharkiv() {
        addNewLocation(1);
    }

    @OnClick(R.id.btnLviv)
    public void onClickBtnLviv() {
        addNewLocation(2);
    }

    @OnClick(R.id.btnOdessa)
    public void onClickBtnOdessa() {
        addNewLocation(3);
    }

    @OnClick(R.id.btnDnipro)
    public void onClickBtnDnipro() {
        addNewLocation(4);
    }

    public void addNewLocation(int id) {
        googleMap.clear();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(City.getCityLocation(id))
                .zoom(10)
                .build();
        googleMap.addMarker(new MarkerOptions().position(City.getCityLocation(id)).title(City.getCityName(id)));
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}
