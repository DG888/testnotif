package dg.com.testnotifications.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import dg.com.testnotifications.R;
import dg.com.testnotifications.adapter.PushNotificationAdapter;
import dg.com.testnotifications.utils.preferences.CorePreferencesImpl;

public class NotificationListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private PushNotificationAdapter pushNotificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        ButterKnife.bind(this);

        setupRecyclerView();

        pushNotificationAdapter.updateData(CorePreferencesImpl.getCorePref().getNotificationList());
    }

    public void setupRecyclerView() {
        pushNotificationAdapter = new PushNotificationAdapter();
        LinearLayoutManager lm = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(pushNotificationAdapter);
    }

}
