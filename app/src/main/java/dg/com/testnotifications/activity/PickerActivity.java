package dg.com.testnotifications.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import dg.com.testnotifications.R;
import dg.com.testnotifications.utils.DateUtils;
import dg.com.testnotifications.utils.dialogs.DatePickerFragment;
import dg.com.testnotifications.utils.dialogs.TimePickerFragment;

public class PickerActivity extends AppCompatActivity {

    @BindView(R.id.textTime) TextView textTime;
    @BindView(R.id.textDate) TextView textDate;
    @BindView(R.id.switch1) Switch switch1;

    private boolean isActive24format = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnTime)
    public void clickBtnTime() {
        TimePickerFragment newFragment = new TimePickerFragment(isActive24format);
        newFragment.setTimeChangeListener(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String timeStr = isActive24format ? DateUtils.getTimeIn24HFormat(hourOfDay, minute) : DateUtils.getTimeIn12HFormat(hourOfDay, minute);
                textTime.setText(timeStr);
            }
        });
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @OnClick(R.id.btnDate)
    public void clickBtnDate() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setDateChangeListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                textDate.setText(DateUtils.getDate(dayOfMonth, month, year));
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @OnCheckedChanged(R.id.switch1)
    public void switchTo12H(){
        if(switch1.isChecked()) {
            isActive24format = false;
        } else {
            isActive24format = true;
        }
    }

}
