package dg.com.testnotifications.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dg.com.testnotifications.R;
import dg.com.testnotifications.enums.Country;
import dg.com.testnotifications.utils.preferences.CorePreferencesImpl;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CorePreferencesImpl.getCorePref().init(getApplicationContext());

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    @OnClick(R.id.buttonList)
    public void clickBtnList() {
        Intent intent = new Intent(this, NotificationListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonMap)
    public void clickBtnMap() {
        Intent intent = new Intent(this, MapsActivityMap.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonPicker)
    public void clickBtnPicker() {
        Intent intent = new Intent(this, PickerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonFacebook)
    public void clickBtnFacebook() {
        Intent intent = new Intent(this, LoginViaFacebookActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonTabLayout)
    public void clickBtnTabLayout() {
        Intent intent = new Intent(this, TabLayoutActivity.class);
        startActivity(intent);
    }

}
