package dg.com.testnotifications.adapter;

import android.app.Notification;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dg.com.testnotifications.R;
import dg.com.testnotifications.model.PushNotification;
import dg.com.testnotifications.utils.DateUtils;

public class PushNotificationAdapter extends RecyclerView.Adapter<PushNotificationAdapter.NotificationViewHolder> {

    private List<PushNotification> notificationList = new ArrayList<>();

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.bind(notificationList.get(position));
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public void updateData(List<PushNotification> notifications){
        this.notificationList.addAll(notifications);
        notifyDataSetChanged();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvMessage) TextView tvMessage;
        @BindView(R.id.tvDate) TextView tvDate;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PushNotification notification){
            tvTitle.setText(notification.getTitle());
            tvMessage.setText(notification.getMessage());
            tvDate.setText(DateUtils.getDateInFullFormat(notification.getDate()));
        }

    }

}
