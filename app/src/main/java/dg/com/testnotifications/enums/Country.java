package dg.com.testnotifications.enums;

import com.google.android.gms.maps.model.LatLng;

import dg.com.testnotifications.R;

public enum Country {

    UKRAINE(0, "Ukraine", 12427, 2344),
    POLAND(1, "Poland", 3852, 123612),
    USA(2, "Usa", 12427, 23446),
    EGYPT(3, "Egypt", 12427, 23446),
    SPAIN(4, "Spain", 12427, 23446);

    private String name;
    private long lat;
    private long lng;

    Country(int id, String name, long lat, long lng) {
        this.name = name;
    }

    public static Country getCountryByName(String name) {
        for (Country country : Country.values()){
            if (country.getName().equals(name)) {
                return country;
            }
        }
        return UKRAINE; //Default value
    }

    public static LatLng getCountryLocation(String name) {
        for (Country country : Country.values()){
            if (country.getName().equals(name)) {
                return new LatLng(country.getLat(), country.getLng());
            }
        }
        return new LatLng(0,0); //Default value
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
