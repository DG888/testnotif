package dg.com.testnotifications.enums;

import com.google.android.gms.maps.model.LatLng;

public enum City {

    KYIV(0, "Kyiv", 50.45466, 30.5238),
    KHARKIV(1, "Kharkiv", 49.98081, 36.25272),
    LVIV(2, "Lviv", 49.83826, 24.02324),
    ODESSA(3, "Odessa", 46.47747, 30.73262),
    DNIPRO(4, "Dnipro", 48.45, 34.98333);

    private int id;
    private String name;
    private double lat;
    private double lng;

    City(int id, String name, double lat, double lng) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public static LatLng getCityLocation(int id) {
        for (City city : City.values()){
            if (city.getId() == id) {
                return new LatLng(city.getLat(), city.getLng());
            }
        }
        return new LatLng(0,0);
    }

    public static String getCityName(int id) {
        for (City city : City.values()){
            if (city.getId() == id) {
                return new String(city.getName());
            }
        }
        return new String();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

}
