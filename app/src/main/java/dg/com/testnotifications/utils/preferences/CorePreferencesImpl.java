package dg.com.testnotifications.utils.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import dg.com.testnotifications.model.PushNotification;

public class CorePreferencesImpl implements CorePreferences {

    private static final String CORE_PREFERENCES = "core.preferences";
    private final String KEY_NOTIFICATION_LIST = "key.notification.list";

    private static CorePreferencesImpl corePreferences;
    private SharedPreferences sharedPreferences;

    public static synchronized CorePreferencesImpl getCorePref() {
        if(corePreferences == null) {
            corePreferences = new CorePreferencesImpl();
        }
        return corePreferences;
    }

    public void init(Context context) {
        if(sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(CORE_PREFERENCES, Context.MODE_PRIVATE);
        }
    }

    @Override
    public void setNotificationList(List<PushNotification> notificationList) {
        Gson gson = new Gson();
        sharedPreferences.edit().putString(KEY_NOTIFICATION_LIST, gson.toJson(notificationList)).apply();
    }

    @Override
    public List<PushNotification> getNotificationList() {
        Gson gson = new Gson();
        String notifList = sharedPreferences.getString(KEY_NOTIFICATION_LIST,"");
        if(notifList.isEmpty()) {
            return new ArrayList<>();
        } else {
            Type listPushNotificationType = new TypeToken<List<PushNotification>>(){}.getType();
            return gson.fromJson(notifList, listPushNotificationType);
        }
    }

}
