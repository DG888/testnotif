package dg.com.testnotifications.utils.preferences;

import java.util.List;

import dg.com.testnotifications.model.PushNotification;

public interface CorePreferences {

    void setNotificationList(List<PushNotification> notificationList);

    List<PushNotification> getNotificationList();

}
