package dg.com.testnotifications.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    private static SimpleDateFormat time24H = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat time12H = new SimpleDateFormat("hh:mm a");
    private static SimpleDateFormat date = new SimpleDateFormat("dd-MMMM-yyyy");

    private static SimpleDateFormat formatFullDate = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");

    public static String getDateInFullFormat(long date) {
        return formatFullDate.format(new Date(date));
    }

    public static String getDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return date.format(calendar.getTime());
    }

    public static String getTimeIn24HFormat(int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        return time24H.format(calendar.getTime());
    }

    public static String getTimeIn12HFormat(int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        return time12H.format(calendar.getTime());
    }

}
